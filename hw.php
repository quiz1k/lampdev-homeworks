<?php

$users["0"] = ["name" => "Alex", "email" => "alex@gmail.com", "lang" => "de"];
$users["10"] = ["name" => "test2", "email" => "nikolay@gmail.com", "lang" => "pl"];
$users["22"] = ["name" => "test2", "email" => "yaroslaw@gmail.com", "lang" => "ua"];
$users["4"] = ["name" => "Vitaliy", "email" => "vitaliy@test.com", "lang" => "fr"];
$users["55"] = ["name" => "Test", "email" => "test@test.com", "lang" => "pl"];
$users["3"] = ["name" => "Anton", "email" => "anton@gmail.com", "lang" => "fr"];

echo "Users are " . count($users);
krsort($users);
echo "<br> Min user: ";
print_r($users[min(array_keys($users))]);
echo "<br>Max user: ";
print_r($users[max(array_keys($users))]);
$sortedKeys = array_keys($users);
krsort($sortedKeys);
current($sortedKeys);
echo "<br>Next min user: ";
print_r(($users[next($sortedKeys)]));
end($sortedKeys);
echo "<br>Prev max user: ";
print_r(($users[prev($sortedKeys)]));
unset($users[min(array_keys($users))]);

$sortedKeys = array_keys($users);
krsort($sortedKeys);
echo "<pre>";
print_r($users);
echo "</pre>";
reset($sortedKeys);
$value1 = current($sortedKeys);
$value2 = end($sortedKeys);

echo "<br>";
if (($users[$value1]["lang"] == "ru") && ($users[$value2]["lang"] == "ru")) {
    echo "Привет!";
} elseif (($users[$value1]["lang"] == "ua") && ($users[$value2]["lang"] == "ua")) {
    echo "Привіт!";
} elseif (($users[$value1]["lang"] == "de") && ($users[$value2]["lang"] == "de")) {
    echo "Hallo!"; 
} elseif (($users[$value1]["lang"] == "fr") && ($users[$value2]["lang"] == "fr")) {
    echo "Bonjour!";
} elseif (($users[$value1]["lang"] == "pl") && ($users[$value2]["lang"] == "pl")) {
    echo "Siema!";
} else {
    if ($users[$value1]["lang"] == "ru") echo "Привет!<br>";
    if ($users[$value1]["lang"] == "ua") echo "Привіт!<br>";
    if ($users[$value1]["lang"] == "de") echo "Hallo!<br>";
    if ($users[$value1]["lang"] == "fr") echo "Bonjour!<br>";
    if ($users[$value1]["lang"] == "pl") echo "Siema!<br>";
    if ($users[$value2]["lang"] == "ru") echo "Привет!<br>";
    if ($users[$value2]["lang"] == "ua") echo "Привіт!<br>";
    if ($users[$value2]["lang"] == "de") echo "Hallo!<br>";
    if ($users[$value2]["lang"] == "fr") echo "Bonjour!<br>";
    if ($users[$value2]["lang"] == "pl") echo "Siema!<br>";
}

$arrayOfNames = [];
foreach ($sortedKeys as $key_value) {
    foreach ($users[$key_value] as $key => $value) {
        if ($key === 'name') {
            array_push($arrayOfNames, $value);
        }
    }
}

foreach (array_count_values($arrayOfNames) as $key => $value) {
    if ($value > 1) {
        echo $key . " repeats " . $value . " times";
    }
}

echo "<br>";
$ArrayRu = [];
$ArrayUa = [];
$ArrayPl = [];
$ArrayFr = [];
$ArrayDe = [];

foreach ($sortedKeys as $key_value) {
    foreach ($users[$key_value] as $key => $value) {
        if ($key === 'lang' && $value === 'ua') {
            array_push($ArrayUa, $users[$key_value]);
        }
        if ($key === 'lang' && $value === 'fr') {
            array_push($ArrayFr, $users[$key_value]);
        }
        if ($key === 'lang' && $value === 'pl') {
            array_push($ArrayPl, $users[$key_value]);
        }
        if ($key === 'lang' && $value === 'de') {
            array_push($ArrayDe, $users[$key_value]);
        }
        if ($key === 'lang' && $value === 'ru') {
            array_push($ArrayRu, $users[$key_value]);
        }
    }
}
echo "Array UA: <br>";
print_r($ArrayUa);
echo "<br> Array Fr: <br>";
print_r($ArrayFr);
echo "<br> Array De: <br>";
print_r($ArrayDe);
echo "<br> Array Pl: <br>";
print_r($ArrayPl);
echo "<br> Array Ru: <br>";
print_r($ArrayRu);
echo "<br>";

$new_users = [];

$keys = array_keys($users);
foreach ($keys as $key_value) {
    array_unshift($new_users, $users[$key_value]);
}

$users2 = array_combine($sortedKeys, $new_users);


echo "<pre>";
print_r($users2);
echo "</pre>";
?>