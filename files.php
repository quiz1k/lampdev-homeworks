<?php

define('ROOT_PATH', dirname(__FILE__));

if (!empty($_POST)) {
    $login = $_POST["login"];
    $password = $_POST["password"];
    $file = fopen(ROOT_PATH . "/file.txt", "r");
    if (!$file) {
        die("File open error!");
    }
    while (!feof($file)) {
        $line = explode(" ", trim(fgets($file)));
        if ($line[0] === $login && $line[1] === $password) {
            echo "Your login = " . $login;
            $newFile = fopen(ROOT_PATH . "/" . $login . ".txt", "c+");
            $newFileLine = explode(" ", trim(fgets($newFile)));
            if ($newFileLine[0] === $login) {
                rewind($newFile);
                fwrite($newFile, $login . " " . $newFileLine[1] + 1);
            } else {
                fwrite($newFile, $login . " 1");
            }
            fclose($newFile);
        }
    }
    fclose($file);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="POST">
        <label>Input login </label> <br>
        <input type="text" name="login"> <br>
        <label>Input pass </label> <br>
        <input type="password" name="password"> <br>
        <button type="submit" value="Submit">Submit</button>
    </form>
</body>
</html>